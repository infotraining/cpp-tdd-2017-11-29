#include <iostream>

#include "application.hpp"
#include "console.hpp"
#include "command.hpp"
#include "di.hpp"

using namespace std;
namespace di = boost::di;

int main()
{
    Terminal terminal;
    Application app(terminal);
    Document doc;

    auto cmd_print = make_shared<PrintCmd>(doc, terminal);
    auto cmd_add_text = make_shared<AddTextCmd>(doc, terminal);

    app.add_command("print", cmd_print);
    app.add_command("addtext", cmd_add_text);

    app.run();
}
