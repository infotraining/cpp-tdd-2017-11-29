#include "gmock/gmock.h"

#include "application.hpp"
#include "mocks/mock_console.hpp"
#include "mocks/mock_command.hpp"

using namespace ::testing;
using namespace std;

struct ApplicationTests : Test
{
    NiceMock<MockConsole> mq_console;
    Application app;

   ApplicationTests() : app{mq_console}
   {}
};

struct ApplicationTests_MainLoop : ApplicationTests
{};

TEST_F(ApplicationTests_MainLoop, GetsLineFromInput)
{   
    EXPECT_CALL(mq_console, print(_)).WillOnce(Return());
    EXPECT_CALL(mq_console, get_line()).WillOnce(Return("exit"));

    app.run();
}

TEST_F(ApplicationTests_MainLoop, GetLineFromInputUntilExit)
{
    string cmd_name = "test";
    string cmd_exit = "exit";

    EXPECT_CALL(mq_console, get_line()).WillOnce(Return(cmd_exit));
    EXPECT_CALL(mq_console, get_line())
            .Times(2)
            .WillRepeatedly(Return(cmd_name))
            .RetiresOnSaturation();

    app.run();
}

TEST_F(ApplicationTests_MainLoop, RegisteredCmdIsExecuted)
{
    auto mq_cmd = make_shared<MockCommand>();

    app.add_command("test", mq_cmd);

    EXPECT_CALL(mq_console, get_line())
        .Times(2).WillOnce(Return("test")).WillOnce(Return("exit"));
    EXPECT_CALL(*mq_cmd, execute()).Times(1);

    app.run();
}

TEST_F(ApplicationTests_MainLoop, UnregisterCmdPrintsErrorMsg)
{
    ::testing::Sequence s;

    EXPECT_CALL(mq_console, print(_)).Times(1).InSequence(s);
    EXPECT_CALL(mq_console, print(Messages::msg_unknown_cmd)).Times(1).InSequence(s);
    EXPECT_CALL(mq_console, print(_)).Times(1).InSequence(s);

    EXPECT_CALL(mq_console, get_line())
        .Times(2).WillOnce(Return("unknown")).WillOnce(Return("exit"));


    app.run();
}

