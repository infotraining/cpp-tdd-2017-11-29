#ifndef MOCK_CONSOLE_HPP
#define MOCK_CONSOLE_HPP

#include "console.hpp"
#include "gmock/gmock.h"

class MockConsole : public Console
{
public:
    MOCK_METHOD0(get_line, std::string());
    MOCK_METHOD1(print, void (const std::string&));
};

#endif // MOCK_CONSOLE_HPP
