#ifndef MOCK_COMMAND_HPP
#define MOCK_COMMAND_HPP

#include "gmock/gmock.h"

struct MockCommand : public Command
{
    MOCK_METHOD0(execute, void());
};

struct MockUndoableCommand : public UndoableCommand
{
    MOCK_METHOD0(execute, void());
    MOCK_METHOD0(undo, void());
    MOCK_CONST_METHOD0(clone, std::unique_ptr<UndoableCommand>());
};

#endif // MOCK_COMMAND_HPP
