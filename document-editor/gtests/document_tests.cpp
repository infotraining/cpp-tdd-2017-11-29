#include "command.hpp"
#include "mocks/mock_console.hpp"
#include "document.hpp"

#include "gtest/gtest.h"



TEST(PrintCmdTests, Execute_PrintsDocInConsole)
{
    Document doc{"abc"};
    MockConsole mq_console;
    PrintCmd print_cmd{doc, mq_console};

    EXPECT_CALL(mq_console, print("[abc]")).Times(1);

    print_cmd.execute();
}

TEST(AddTextCmdTests, Execute_GetsTextAndAddsToDoc)
{
    using namespace ::testing;

    Document doc{"abc"};
    MockConsole mq_console;

    AddTextCmd add_text{doc, mq_console};

    EXPECT_CALL(mq_console, print(_)).Times(1);
    EXPECT_CALL(mq_console, get_line()).WillOnce(Return("def"));

    add_text.execute();

    ASSERT_EQ(doc.text(), "abcdef");
}
