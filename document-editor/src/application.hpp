#ifndef APPLICATION_HPP
#define APPLICATION_HPP

#include <unordered_map>

#include "console.hpp"
#include "command.hpp"

namespace Messages
{
    constexpr auto msg_unknown_cmd = "Unknown command: ";
    constexpr auto msg_prompt = "Enter a command: ";
};

namespace Commands
{
    constexpr auto cmd_exit = "exit";
};

class Application
{
    Console& console_;
    std::unordered_map<std::string, std::shared_ptr<Command>> commands_;
public:
    Application(Console& console)
        : console_{console}
    {}

    void add_command(const std::string& name, std::shared_ptr<Command> cmd)
    {
        commands_.insert(std::make_pair(name, cmd));
    }

    void run()
    {
        while(true)
        {
            console_.print(Messages::msg_prompt);

            auto input_text = console_.get_line();

            if (input_text == Commands::cmd_exit)
                break;

            if (is_registered_command(input_text))
                commands_[input_text]->execute();
            else
                console_.print(Messages::msg_unknown_cmd);
        }
    }
private:
    bool is_registered_command(const std::string& cmd) const
    {
        return commands_.count(cmd);
    }
};

#endif // APPLICATION_HPP
