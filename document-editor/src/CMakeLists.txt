# library files files
file(GLOB LIB_SOURCES "./*.cpp")
file(GLOB LIB_HEADERS "./*.hpp" "./*.h")

add_library(${PROJECT_LIB_NAME} ${LIB_SOURCES} ${LIB_HEADERS})
target_include_directories(${PROJECT_LIB_NAME} PUBLIC "./")
target_link_libraries(${PROJECT_LIB_NAME} ${Boost_LIBRARIES})