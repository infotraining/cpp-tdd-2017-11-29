#ifndef COMMAND_HPP
#define COMMAND_HPP

#include "clipboard.hpp"
#include "console.hpp"
#include "document.hpp"
#include <memory>
#include <stack>

class Command
{
public:
    virtual void execute() = 0;
    virtual ~Command() = default;
};

using CommandSharedPtr = std::shared_ptr<Command>;
using CommandPtr = std::unique_ptr<Command>;

class UndoableCommand : public Command
{
public:
    virtual void undo() = 0;
    virtual std::unique_ptr<UndoableCommand> clone() const = 0;
};

using UndoableCommandPtr = std::unique_ptr<UndoableCommand>;

template <typename Cmd, typename BaseCommand = UndoableCommand>
class CloneableCommand : public BaseCommand
{
public:
    std::unique_ptr<UndoableCommand> clone() const override
    {
        return std::make_unique<Cmd>(static_cast<Cmd const&>(*this));
    }
};

class CommandHistory
{
    std::stack<UndoableCommandPtr> history_;

public:
    void record_last_command(UndoableCommandPtr cmd)
    {
        history_.push(std::move(cmd));
    }

    UndoableCommandPtr pop_last_command()
    {

        if (history_.empty())
            throw std::out_of_range("Command history is empty");
        auto last_cmd = std::move(history_.top());
        history_.pop();

        return last_cmd;
    }
};

template <typename CommandType, typename CommandBaseType = UndoableCommand>
class UndoableCommandBase : public CloneableCommand<CommandType, CommandBaseType>
{
    CommandHistory& history_;

public:
    UndoableCommandBase(CommandHistory& history) : history_{history}
    {
    }

    void execute() final override
    {
        do_save_state();
        history_.record_last_command(this->clone());
        do_execute();
    }

    void undo() final override
    {
        do_undo();
    }

protected:
    virtual void do_save_state() = 0;
    virtual void do_execute() = 0;
    virtual void do_undo() = 0;
};

class PrintCmd : public Command
{
    Document& doc_;
    Console& console_;
public:
    PrintCmd(Document& doc, Console& console)
        : doc_{doc}, console_{console}
    {}

    void execute() override
    {
        console_.print("[" + doc_.text() + "]");
    }
};

class AddTextCmd : public Command
{
    Document& doc_;
    Console& console_;
public:
    AddTextCmd(Document& doc, Console& console)
        : doc_{doc}, console_{console}
    {}

    void execute() override
    {
        console_.print("Get text: ");
        auto txt = console_.get_line();
        doc_.add_text(txt);
    }
};

#endif // COMMAND_HPP
