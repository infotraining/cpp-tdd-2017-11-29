#include "bowling_game.hpp"
#include <iostream>

using namespace std;

int main()
{
    BowlingGame bowling_game;

    for(size_t i = 0; i < 12; ++i)
        bowling_game.roll(10);

    cout << "Pefect game score: " << bowling_game.score() << endl;
}
