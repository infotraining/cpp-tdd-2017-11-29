#define CATCH_CONFIG_RUNNER

#include <catch.hpp>
#include <string>
#include <trompeloeil.hpp>

#include "application.hpp"
#include "mocks/mock_console.hpp"

using namespace std;
using namespace trompeloeil;

TEST_CASE("Application start", "[app]")
{
    MockConsole mq_console;
    REQUIRE_CALL(mq_console, print("App"));

    Application app(mq_console);
    app.run();
}

int main(int argc, char** argv)
{
    // global setup...

    trompeloeil::set_reporter([](::trompeloeil::severity s,
                                  char const* file,
                                  unsigned long line,
                                  const std::string& msg) {
        std::ostringstream os;
        if (line)
            os << file << ':' << line << '\n';
        os << msg;
        if (s == ::trompeloeil::severity::fatal)
        {
            FAIL(os.str());
        }
        CHECK(os.str() == "");
    });

    int result = Catch::Session().run(argc, argv);

    // global clean-up...

    return result;
}
