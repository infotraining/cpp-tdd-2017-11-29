#include <iostream>

#include "src/application.hpp"
#include "di/di.hpp"

using namespace std;
namespace di = boost::di;

int main()
{
    const auto injector = di::make_injector(
        di::bind<Console>.to<Terminal>()        
    );

    auto app = injector.create<Application>();
    
    app.run();
}
