#ifndef _HPP
#define _HPP

#include <array>
#include <tuple>

inline std::string full_name(const std::string& first_name, const std::string& last_name)
{
    return first_name + " " + last_name;
}

#endif