#include <algorithm>
#include <vector>
#include <list>

using namespace std;

#include "gtest/gtest.h"
#include "gmock/gmock.h"

TEST(GTestDemo, LogicAsserts)
{
    int x = 10;

    //EXPECT_TRUE(x == 0); // non-critical assertions

    ASSERT_TRUE(x == 10);
    ASSERT_FALSE(x < 5);
}

TEST(GTestDemo, ComparingAsserts)
{
    int x = 10;

    ASSERT_EQ(x, 10);
    ASSERT_NE(x, 11);
    ASSERT_GT(x, 5);
    ASSERT_LT(x, 20);
    ASSERT_GE(x, 10);
    ASSERT_LE(x, 10);
}

TEST(GTestDemo, FloatingAsserts)
{
    double pi = 3.140000000000000001;

    EXPECT_DOUBLE_EQ(pi, 3.14);
    ASSERT_NEAR(pi, 3.14, 0.01);
}

TEST(GTestDemo, ThrowingExceptionsAsserts)
{
    vector<int> vec = { 1, 2, 3 };

    ASSERT_THROW(vec.at(10), out_of_range);

    ASSERT_ANY_THROW(vec.at(10));

    ASSERT_NO_THROW(vec.at(0));
}


class VectorTests : public ::testing::Test
{
protected:
    vector<int> vec;

public:
    VectorTests() : vec{1, 2, 3}
    {}
};

TEST_F(VectorTests, ResizeChangesSize)
{
    vec.resize(10);

    ASSERT_EQ(vec.size(), 10);
}

TEST_F(VectorTests, ClearChangesSizeToZero)
{
    vec.clear();

    ASSERT_EQ(vec.size(), 0);
}

class ListTests : public ::testing::Test
{
protected:
    list<int> lst;

    void SetUp()
    {
        lst.insert(lst.begin(), { 1, 2, 3});
    }

    void TearDown()
    {
        lst.clear();
    }
};

TEST_F(ListTests, PushFrontInsertsItemAtFront)
{
    lst.push_front(4);

    ASSERT_EQ(lst.front(), 4);
}


TEST(AssertsWithMatchers, SimpleMatchers)
{
    int x = 10;

    ASSERT_THAT(x, ::testing::Eq(10));

    using namespace ::testing;

    ASSERT_THAT(x, Gt(5));
    ASSERT_THAT(x, AllOf(Gt(5), Lt(15)));
}

TEST(AssertsWithMatchers, PointerMatchers)
{
    using namespace  ::testing;

    int* ptr = nullptr;
    ASSERT_THAT(ptr, IsNull());

    int x = 10;
    ptr = &x;
    ASSERT_THAT(ptr, NotNull());

    const char* txt = "test";
    ASSERT_THAT(txt, AllOf(NotNull(), Not(StrEq(""))));
}

TEST(AssertsWithMatchers, StringMatchers)
{
    using namespace ::testing;

    string txt = "test of matcher";

    ASSERT_THAT(txt, StartsWith("test"));
    ASSERT_THAT(txt, EndsWith("matcher"));
    ASSERT_THAT(txt, StrEq("test of matcher"));
    ASSERT_THAT(txt, StrNe("test of Matcher"));
    ASSERT_THAT(txt, StrCaseEq("test of Matcher")); // case insensitive eq
}

class VectorTestWithMatchers : public VectorTests
{
};

TEST_F(VectorTestWithMatchers, ContainerMatchers)
{
    using namespace ::testing;

    vector<int> expected = { 1, 2, 3 };

    ASSERT_THAT(vec, Not(IsEmpty()));
    ASSERT_THAT(vec, ContainerEq(expected));
    ASSERT_THAT(vec, ElementsAre(1, 2, 3));
    ASSERT_THAT(vec, ElementsAre(Gt(0), 2, Le(3)));


    vector<int> shuffled = { 3, 1, 2 };
    ASSERT_THAT(shuffled, UnorderedElementsAre(1, 2, 3));
}
