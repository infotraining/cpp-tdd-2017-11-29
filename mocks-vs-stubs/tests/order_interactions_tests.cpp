#include <algorithm>
#include <memory>
#include <string>
#include <unordered_map>

#include "warehouse.hpp"
#include "order.hpp"
#include "gtest/gtest.h"
#include "gmock/gmock.h"

using namespace std;

struct MockWarehouse : Warehouse
{
    MOCK_CONST_METHOD2(has_inventory, bool (const std::string&, size_t));
    MOCK_METHOD2(add, void (const std::string&, size_t ));
    MOCK_METHOD2(remove, void (const std::string&, size_t));
    MOCK_CONST_METHOD1(get_inventory, size_t (const std::string&));
};

using ::testing::Return;
using ::testing::_;

class OrderInteractionsTests : public ::testing::Test
{
protected:
    const string talisker = "Talisker";
    const string highland_park = "Highland Park";

    MockWarehouse warehouse_;

public:
    OrderInteractionsTests() = default;
};

TEST_F(OrderInteractionsTests, FillingOrderRemovesInventoryIfEnoughInStock)
{
    Order order{talisker, 50};

    ::testing::Sequence s;

    EXPECT_CALL(warehouse_, has_inventory(talisker, 50)).InSequence(s).WillOnce(Return(true));
    EXPECT_CALL(warehouse_, remove(talisker, 50)).Times(1).InSequence(s);

    order.fill(warehouse_);
}

TEST_F(OrderInteractionsTests, OrderIsFilledIfEnoughInStock)
{
    Order order{talisker, 50};

    ON_CALL(warehouse_, has_inventory(talisker, 50)).WillByDefault(Return(true));
    EXPECT_CALL(warehouse_, remove(_, _));

    order.fill(warehouse_);

    ASSERT_TRUE(order.is_filled());
}

TEST_F(OrderInteractionsTests, FillingOrderDoesNotRemoveInventoryIfNotEnoughInStock)
{
    Order order{talisker, 50};

    ON_CALL(warehouse_, has_inventory(talisker, 50)).WillByDefault(Return(false));
    EXPECT_CALL(warehouse_, remove(_, _)).Times(0);

    order.fill(warehouse_);
}

TEST_F(OrderInteractionsTests, OrderIsNotFilledIfNotEnoughInStock)
{
    Order order{talisker, 50};

    EXPECT_CALL(warehouse_, has_inventory(talisker, 50)).WillOnce(Return(false));

    order.fill(warehouse_);

    ASSERT_FALSE(order.is_filled());
}
