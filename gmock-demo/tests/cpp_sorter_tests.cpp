#include <algorithm>
#include <string>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

using namespace std;

class Interface
{
public:
    virtual ~Interface() = default;

    virtual std::string get_name() const = 0;
    virtual std::string get_value(int x) = 0;
    virtual std::string get_value(int x) const = 0;
    virtual bool save_value(int k, const string& v) = 0;
    virtual std::vector<int> get_data(const std::string& filename) const = 0;
    virtual int generate() = 0;
};

struct MockInterface : Interface
{
    MOCK_CONST_METHOD0(get_name, std::string());
    MOCK_METHOD1(get_value, std::string(int));
    MOCK_CONST_METHOD1(get_value, string(int));
    MOCK_METHOD2(save_value, bool(int, const std::string&));
    MOCK_CONST_METHOD1(get_data, vector<int>(const std::string&));
    MOCK_METHOD0(generate, int());
};

struct GMockDemoTests : ::testing::Test
{
    ::testing::NiceMock<MockInterface> mock;

    void SetUp()
    {
        ::testing::DefaultValue<int>::Set(42);
    }

    void TearDown()
    {
        ::testing::DefaultValue<int>::Clear();
    }
};

TEST_F(GMockDemoTests, DefaultValuesCanBeSetForFixture)
{
    ASSERT_EQ(mock.generate(), 42);
}

TEST_F(GMockDemoTests, DefaultValueCanBeSetForMethodUsingOnCall)
{
    ON_CALL(mock, generate()).WillByDefault(::testing::Return(665));

    ASSERT_EQ(mock.generate(), 665);
}

TEST_F(GMockDemoTests, DefaultValueCanBeSetForMethodUsingExpectCall)
{
    EXPECT_CALL(mock, get_name()).WillRepeatedly(::testing::Return("jan"));

    ASSERT_EQ(mock.get_name(), "jan");
    ASSERT_EQ(mock.get_name(), "jan");
    ASSERT_EQ(mock.get_name(), "jan");

    ASSERT_EQ(mock.get_value(1), ""s);
}

TEST_F(GMockDemoTests, VerificationHowManyTimesMethodIsCalled)
{
    using namespace ::testing;

    EXPECT_CALL(mock, get_name()).WillOnce(Return("adam"));

    ASSERT_EQ(mock.get_name(), "adam");
    //ASSERT_EQ(mock.get_name(), ""); // error - called twice

    EXPECT_CALL(mock, save_value(_, _)).WillRepeatedly(Return(false));
    EXPECT_CALL(mock, save_value(Gt(0), _)).Times(AtMost(3)).WillRepeatedly(Return(true)).RetiresOnSaturation();

    ASSERT_TRUE(mock.save_value(1, "a"));
    mock.save_value(2, "b");
    ASSERT_TRUE(mock.save_value(3, "c"));
    mock.save_value(4, "d");
}

TEST_F(GMockDemoTests, ReturnDifferentValuesBasedOnArgument)
{
    using namespace ::testing;

    EXPECT_CALL(mock, get_value(Gt(0))).WillRepeatedly(Return("positive"));
    EXPECT_CALL(mock, get_value(Lt(0))).WillRepeatedly(Return("negative"));

    ASSERT_EQ(mock.get_value(10), "positive");
    ASSERT_EQ(mock.get_value(-1), "negative");
}

TEST_F(GMockDemoTests, SpyingOnParamtersInvokedInMock)
{
    using namespace ::testing;

    vector<int> spy;

    EXPECT_CALL(mock, get_value(_))
            .WillRepeatedly(Invoke([&spy](int arg) { spy.push_back(arg); return to_string(arg); }));

    ASSERT_THAT(mock.get_value(1), StrEq("1"));
    ASSERT_THAT(mock.get_value(2), StrEq("2"));
    ASSERT_THAT(mock.get_value(3), StrEq("3"));

    ASSERT_THAT(spy, ElementsAre(1, 2, 3));
}

TEST_F(GMockDemoTests, ReturningSequencedValues)
{
    using namespace ::testing;

    vector<string> data = { "zero", "one", "two", "three" };

    EXPECT_CALL(mock, get_value(_))
            .WillRepeatedly((Invoke([&data](int i) { return data[i];})));

    ASSERT_THAT(mock.get_value(0), StrEq("zero"));
    ASSERT_THAT(mock.get_value(2), StrEq("two"));
}

TEST_F(GMockDemoTests, ThrowingAnException)
{
    using namespace ::testing;

    invalid_argument excpt("Invalid arg");
    EXPECT_CALL(mock, get_value(Lt(0))).WillRepeatedly(Throw(excpt));

    ASSERT_THROW(mock.get_value(-2), invalid_argument);
}

class Lambda_234278345237854
{
public:
    int operator()() const
    {
        return 42;
    }
};

class Lambda_2342rwer78345237854
{
    int value_;
public:
    Lambda_2342rwer78345237854(int value) : value_{value}
    {}

    int operator()(int x) const
    {
        return x * value_;
    }
};

TEST(LambdaTests, BasicUsage)
{
    auto l1 = []() { return 42; };
    ASSERT_EQ(l1(), 42);

    auto l2 = [](int x, int y) { return x + y; };
    ASSERT_EQ(l2(1, 2), 3);

    int value = 10;
    auto l3 = [value](int x) { return x * value; };
    value = 100;
    ASSERT_EQ(l3(3), 30);

    auto l4 = [&value](int x) { return x * value; };
    value = 1000;
    ASSERT_EQ(l4(3), 3000);
}
