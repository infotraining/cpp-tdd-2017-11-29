#include <algorithm>

#include "recently_used_list.hpp"
#include "gtest/gtest.h"
#include "gmock/gmock.h"

class RecentlyUsedList
{
    std::deque<std::string> lst_;
    size_t capacity_ = std::numeric_limits<size_t>::max();
public:
    using value_type = std::string;
    using const_iterator = std::deque<std::string>::const_iterator;

    RecentlyUsedList() = default;

    RecentlyUsedList(size_t capacity)
        : capacity_{capacity}
    {
    }

    bool empty() const
    {
        return lst_.empty();
    }

    size_t size() const
    {
        return lst_.size();
    }

    void add(const std::string& item)
    {
        if (item.empty())
        {
            throw std::invalid_argument("Empty strings are not allowed");
        }

        auto where_item = std::find(lst_.begin(), lst_.end(), item);

        if (where_item != lst_.end())
            std::rotate(lst_.begin(), where_item, where_item + 1);
        else
        {
            if (size() == capacity())
                lst_.pop_back();

            lst_.push_front(item);
        }
    }

    const std::string& front() const
    {
        return lst_.front();
    }

    const std::string& operator[](size_t index) const
    {
        return lst_[index];
    }

    void clear()
    {
        lst_.clear();
    }

    const_iterator begin() const
    {
        return lst_.begin();
    }

    const_iterator end() const
    {
        return lst_.end();
    }

    size_t capacity() const
    {
        return capacity_;
    }
};

using namespace ::testing;

TEST(RecentlyUsedList_DefaultConstructor, ListIsEmpty)
{
    RecentlyUsedList rul;

    ASSERT_THAT(rul, IsEmpty());
}

TEST(RecentlyUsedList_AddingItem, IsNotEmpty)
{
    RecentlyUsedList rul;

    rul.add("item1");

    ASSERT_FALSE(rul.empty());
}

TEST(RecentlyUsedList_AddingItem, IncreasesSize)
{
    RecentlyUsedList rul;

    rul.add("item1");
    rul.add("item2");

    ASSERT_THAT(rul.size(), Eq(2u));
}

TEST(RecentlyUsedList_Clear, RemovesAllItems)
{
    RecentlyUsedList rul;
    rul.add("item1");

    rul.clear();

    ASSERT_THAT(rul, IsEmpty());
}

TEST(RecentlyUsedList_AddingItem, AddingEmptyStringThrowsAnException)
{
    RecentlyUsedList rul;

    ASSERT_THROW(rul.add(""), std::invalid_argument);
}


struct RecentlyUsedList_WithItems : Test
{
    RecentlyUsedList rul;

    void SetUp()
    {
        rul.add("item1");
        rul.add("item2");
        rul.add("item3");
    }

    void TearDown()
    {
        rul.clear();
    }
};


struct RecentlyUsedList_LIFO : RecentlyUsedList_WithItems
{};

TEST_F(RecentlyUsedList_LIFO, LastAddedItemIsAtFront)
{
    rul.add("item4");

    ASSERT_THAT(rul.front(), Eq("item4"));
}


struct RecentlyUsedList_Indexing : RecentlyUsedList_WithItems
{};

TEST_F(RecentlyUsedList_Indexing, ItemsCanBeLookedUpByIndex)
{
    ASSERT_THAT(rul[0], Eq("item3"));
    ASSERT_THAT(rul[1], Eq("item2"));
    ASSERT_THAT(rul[2], Eq("item1"));
}


struct RecentlyUsedList_UniqueItems : RecentlyUsedList_WithItems
{};

TEST_F(RecentlyUsedList_UniqueItems, WhenAddingDuplicateItemIsMovedToFront)
{
    EXPECT_THAT(rul, ElementsAre("item3", "item2", "item1"));

    rul.add("item2");

    ASSERT_THAT(rul, ElementsAre("item2", "item3", "item1"));
}

TEST(RecentlyUsedList_BoundedCapacity, DefualtConstructor_CapcityIsMax)
{
    RecentlyUsedList rul;

    ASSERT_THAT(rul.capacity(), Eq(std::numeric_limits<size_t>::max()));
}

TEST(RecentlyUsedList_BoundedCapacity, ConstructorWithArg_CapacityIsSet)
{
    RecentlyUsedList rul(5);

    ASSERT_THAT(rul.capacity(), Eq(5u));
}

TEST(RecentlyUsedList_BoundedCapacity, WhenListIsFullAddingUniqueItemDropsItemAtBack)
{
   RecentlyUsedList rul(3);

   rul.add("item1");
   rul.add("item2");
   rul.add("item3");

   EXPECT_THAT(rul.size(), Eq(rul.capacity()));

   rul.add("item4");

   ASSERT_THAT(rul, ElementsAre("item4", "item3", "item2"));
}
