#ifndef LED_SWITCH_HPP
#define LED_SWITCH_HPP

#include "led_light.hpp"
#include <memory>

namespace DIP
{
    inline namespace DependencyInjection
    {

        class LEDSwitch : public DIP::ISwitch
        {
            ILEDLight& led_light_;

        public:
            LEDSwitch(ILEDLight& led)
                : led_light_{led}
            {
            }

            void on() override
            {
                led_light_.set_rgb(255, 255, 255);
            }

            void off() override
            {
                led_light_.set_rgb(0, 0, 0);
            }
        };
    }

    namespace TemplateMethod
    {
        class LEDSwitch : public DIP::ISwitch
        {
            std::unique_ptr<ILEDLight> led_light_;

        public:
            LEDSwitch()
            {
                led_light_ = create_led_light();
            }

            void on() override
            {
                led_light_->set_rgb(255, 255, 255);
            }

            void off() override
            {
                led_light_->set_rgb(0, 0, 0);
            }

        protected:
            virtual std::unique_ptr<ILEDLight> create_led_light()
            {
                return std::make_unique<LEDLight>();
            }
        };
    }

    namespace InjectionAsTemplateParameter
    {
        template <typename LED>
        class LEDSwitch : public DIP::ISwitch
        {
            LED led_light_;

        public:
            LEDSwitch()
            {
            }

            void on() override
            {
                led_light_.set_rgb(255, 255, 255);
            }

            void off() override
            {
                led_light_.set_rgb(0, 0, 0);
            }

            LED& led()
            {
                return led_light_;
            }
        };
    }
}

#endif //LED_SWITCH_HPP
