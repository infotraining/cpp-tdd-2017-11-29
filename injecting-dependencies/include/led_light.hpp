#ifndef LED_LIGHT_HPP
#define LED_LIGHT_HPP

#include <iostream>

class ILEDLight
{
public:
    virtual ~ILEDLight() = default;
    virtual void set_rgb(int r, int g, int b) = 0;
};

class LEDLight : public ILEDLight
{
public:
    void set_rgb(int r, int g, int b) override
    {
        std::cout << "Setting(" << r << ", " << g << ", " << b << ")\n";
    }
};

#endif //LED_LIGHT_HPP
