#ifndef BUTTON_HPP
#define BUTTON_HPP

#include "led_light.hpp"
#include "switch.hpp"

namespace DIP
{
    namespace ver1
    {
        class Button
        {
            ILEDLight& led_;
            bool is_on_;

        public:
            Button(ILEDLight& led)
                : led_{led}
                , is_on_{false}
            {
            }

            void click()
            {
                if (not is_on_)
                {
                    led_.set_rgb(255, 255, 255);
                    is_on_ = true;
                }
                else
                {
                    led_.set_rgb(0, 0, 0);
                    is_on_ = false;
                }
            }
        };
    }

    inline namespace ver2
    {
        class Button
        {
            DIP::ISwitch& light_switch_;
            bool is_on_;

        public:
            Button(ISwitch& ls)
                : light_switch_{ls}
                , is_on_{false}
            {
            }

            void click()
            {
                if (not is_on_)
                    light_switch_.on();
                else
                    light_switch_.off();

                is_on_ = not is_on_;
            }
        };
    }
};

#endif
