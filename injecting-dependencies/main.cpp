#include "button.hpp"
#include "led_switch.hpp"
#include <iostream>

using namespace std;

int main()
{
    using namespace DIP;

    LEDLight led;
    DependencyInjection::LEDSwitch led_switch(led);
    ver2::Button btn(led_switch);

    btn.click();
    btn.click();
    btn.click();
    btn.click();
    btn.click();
    btn.click();

    cout << "\n\n\n";

    TemplateMethod::LEDSwitch led_switch2;
    ver2::Button btn2(led_switch2);

    btn2.click();
    btn2.click();
    btn2.click();

    cout << "\n\n\n";

    using LightSwitch = InjectionAsTemplateParameter::LEDSwitch<LEDLight>;
    LightSwitch led_switch3;
    ver2::Button btn3(led_switch3);

    btn3.click();
    btn3.click();
    btn3.click();
}
