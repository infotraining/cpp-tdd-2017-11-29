#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "button.hpp"
#include "led_switch.hpp"
#include "switch.hpp"
#include <tuple>
#include <vector>

using namespace std;
using namespace DIP;

class LEDSpy : public ILEDLight
{
public:
    vector<tuple<int, int, int>> set_rgb_calls;

    void set_rgb(int r, int g, int b) override
    {
        set_rgb_calls.push_back(make_tuple(r, g, b));
    }
};

void ASSERT_RGB_ALL_EQ(int r, int g, int b, int value)
{
    ASSERT_EQ(r, value);
    ASSERT_EQ(g, value);
    ASSERT_EQ(b, value);
}

struct ButtonVer1TestsWithSpy : ::testing::Test
{
    LEDSpy led;
    ver1::Button btn;

    ButtonVer1TestsWithSpy()
        : btn{led}
    {
    }
};

TEST_F(ButtonVer1TestsWithSpy, ClickTurnsLightOn)
{
    // Act
    btn.click();

    // Assert
    int r, g, b;
    tie(r, g, b) = led.set_rgb_calls.back();

    ASSERT_RGB_ALL_EQ(r, g, b, 255);
}

TEST_F(ButtonVer1TestsWithSpy, SecondClickTurnsLightOff)
{
    // Arrange
    btn.click();

    // Act
    btn.click();

    // Assert
    int r, g, b;
    tie(r, g, b) = led.set_rgb_calls.back();

    ASSERT_RGB_ALL_EQ(r, g, b, 0);
}

//////////////////////////////////////////////////////////
/// \brief Tests with mocking switch
///

class MockSwitch : public DIP::ISwitch
{
public:
    MOCK_METHOD0(on, void());
    MOCK_METHOD0(off, void());
};

TEST(ButtonVer2Tests, ClickTurnsLightOn)
{
    MockSwitch mswitch;
    DIP::ver2::Button btn(mswitch);

    EXPECT_CALL(mswitch, on()).Times(1);

    btn.click();
}

TEST(ButtonVer2Tests, SecondClickTurnsLightOff)
{
    ::testing::NiceMock<MockSwitch> mswitch;
    DIP::ver2::Button btn(mswitch);
    btn.click();

    EXPECT_CALL(mswitch, off()).Times(1);

    btn.click();
}

struct MockLedLight : public LEDLight
{
    MOCK_METHOD3(set_rgb, void(int, int, int));
};

TEST(LedSwitchTests, OnSetsRGBToFFF)
{
    MockLedLight mled;
    LEDSwitch ls(mled);
    EXPECT_CALL(mled, set_rgb(255, 255, 255));

    ls.on();
}

TEST(LedSwitchTests, OffSetsRGBTo000)
{
    MockLedLight mled;
    LEDSwitch ls(mled);
    EXPECT_CALL(mled, set_rgb(0, 0, 0));

    ls.off();
}

TEST(TemplatedLedSwitchTests, OnSetsRGBToFFF)
{
    InjectionAsTemplateParameter::LEDSwitch<MockLedLight> ls;
    EXPECT_CALL(ls.led(), set_rgb(255, 255, 255));

    ls.on();
}

TEST(TemplatedLedSwitchTests, OffSetsRGBTo000)
{
    InjectionAsTemplateParameter::LEDSwitch<MockLedLight> ls;
    EXPECT_CALL(ls.led(), set_rgb(0, 0, 0));

    ls.off();
}
