#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "sum.hpp"
#include <functional>

using namespace std;

std::string full_name(const std::string& fn , const std::string& ln)
{
    return fn + " " + ln;
}

class NameFormatter
{
public:
    string operator()(const string& fn, const string& ln) const
    {
        return fn + "@" + ln;
    }
};

TEST_CASE("std::function")
{
    std::function<string (const string&, const string&)> f;

    SECTION("can store a function pointer")
    {
        f = &full_name;

        SECTION("can call a function using function pointer")
        {
            auto result = f("Jan", "Kowalski");

            REQUIRE(result == "Jan Kowalski");
        }
    }

    SECTION("can store a functor")
    {
        NameFormatter formatter;

        f = formatter;

        SECTION("can call an operator() using a functor")
        {
            auto result = f("Jan", "Kowalski");

            REQUIRE(result == "Jan@Kowalski");
        }
    }

    SECTION("can store lambda")
    {
        f = [](const string& fn, const string& ln) { return fn + "^" + ln; };

        SECTION("can be called as function")
        {
            auto result = f("Jan", "Kowalski");

            REQUIRE(result == "Jan^Kowalski");
        }
    }
}

